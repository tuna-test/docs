#Laravel Git
1. Ignore storage and vendor folder
2. Put a zip file of storage and vendor folder 
3. Use the attached theme for the task (AdminTemplete.zip)
4. Send the git URL of the finished task

#Task - Create a backend for employee system
#Setup
1. Use Laravel version 6.x
2. Use storage for saving files
3. Use migrations for database
4. Use default Laravel authentication for login and register

#Tables
1. employee (id, first_name, last_name, email, dob, photo, created_at, updated_at )
2. employee_projects (id, employee_id, project_name, created_at, updated_at)

#Use Laravel data tables for listing page

#Employee Page
1. Add and update employees in a single page
2. List all employees (first_name, last_name, email, dob, photo)
3. Add jquery validate 
4. All fields are required, validate email
5. Use calendar for date of birth (dob)
6. Date of birth cannot be a past date
7. Need to Preview the Photo when choosing the image 


#Employee Projects Page
1. Add and update projects in a single page (assign employees while adding or updating)
2. List all projects with employee name (Use join)
 
#Please add a Laravel relationships in modal


#We have added demo pages in template admin_template/materialadmin/dashboards
#Max Hours : 6
